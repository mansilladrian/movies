import requests
from rest_framework import request, status
from django.conf import settings

# Django REST framework
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.utils import json

# Models
from movies.models import Movie

# Serializers
from movies.serializers import MovieApiSerializer, MovieSerializerValidator, MovieSerializer

# Utils
from movies.utils import calculate_average, calculate_max_value


@api_view(['GET'])
def character_viewset(request, id):
    url_requested = '{base}{id}/'.format(base=settings.BASE_API_URL, id=id)
    character_data = requests.get(url_requested)
    character_json = json.loads(character_data.text)
    homeworld_data = requests.get(character_json['homeworld'])
    homeworld_json = json.loads(homeworld_data.text)
    homeworld_json['residents']=len(homeworld_json['residents'])
    species = []
    for url_species in character_json['species']:
        species_data = requests.get(url_species)
        species_json = json.loads(species_data.text)
        species.append(species_json['name'])

    movie = Movie.objects.filter(character_id=id)
    max_value = 0
    average_value = 0
    if movie.exists():
        average_value = calculate_average(movie.latest('id'))
        max_value = calculate_max_value(movie.latest('id'))

    character_json['homeworld'] = homeworld_json
    character_json['species_name'] = ','.join(map(str, species))
    character_json['average_rating'] = average_value
    character_json['max_rating'] = max_value
    serialize_data = MovieApiSerializer(character_json)
    return Response(serialize_data.data)


@api_view(['POST'])
def save_character_value(request, id):
    """Save a new character on the database"""
    serializer = MovieSerializerValidator(data=request.data)
    if serializer.is_valid(raise_exception=True):
        movie = Movie.objects.create(
            character_id=id,
            value=serializer.data['value']
        )
    return Response(MovieSerializer(movie).data, status=status.HTTP_201_CREATED)
