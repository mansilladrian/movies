from django.db.models import Avg, Max

from movies.models import Movie


def calculate_average(movie):
    """Calculate average value for movie"""
    return Movie.objects.filter(character_id=movie.character_id).aggregate(average=Avg('value'))['average']


def calculate_max_value(movie):
    """Calculate max value for movie"""
    return Movie.objects.filter(character_id=movie.character_id).aggregate(max=Max('value'))['max']
