from rest_framework import serializers

from movies.models import Movie


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = '__all__'

class MovieSerializerValidator(serializers.Serializer):
    """Movie serializer for validate value number"""
    value = serializers.IntegerField(
        required=True,
        min_value=1,
        max_value=5
    )


class WorldApiSerializer(serializers.Serializer):
    """World data serializers"""
    name = serializers.CharField()
    population = serializers.CharField()
    residents = serializers.CharField()


class MovieApiSerializer(serializers.Serializer):
    """Movie data serializer"""
    name = serializers.CharField()
    height = serializers.IntegerField()
    mass = serializers.IntegerField()
    hair_color = serializers.CharField()
    skin_color = serializers.CharField()
    eye_color = serializers.CharField()
    birth_year = serializers.CharField
    gender = serializers.CharField()
    homeworld = WorldApiSerializer()
    species_name = serializers.CharField()
    average_rating = serializers.FloatField()
    max_rating = serializers.FloatField()
