from django.db import models
from django.db.models import Avg, Max


class Movie(models.Model):
    created = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)
    character_id = models.IntegerField()
    value = models.IntegerField()

    def __str__(self):
        return self.value
