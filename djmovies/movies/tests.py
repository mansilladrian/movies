from random import randrange

import requests
from django.test import TestCase
from django.conf import settings
from rest_framework import status
from rest_framework.utils import json

from movies.models import Movie
from movies.utils import calculate_average, calculate_max_value
from movies.views import character_viewset


class EndPointWorkingTest(TestCase):
    """Testing if the endpoit is working"""
    def setUp(self):
        pass
        """It does not need any configurations"""
        # self.url = '{base}{id}'.format(base=settings.BASE_API_URL, id=1)

    def test_get_charters(self):
        """Testing if charters view works"""
        get_request = requests.get('http://localhost:8000/character/3')
        self.assertEqual(get_request.status_code, status.HTTP_200_OK)

    def test_post_value(self):
        rating = randrange(1, 6, 1)
        payload = {
            'value': rating,
        }
        url = 'http://localhost:8000/character/3/rating/'
        post_request = requests.post(url=url, data=payload)
        self.assertEqual(post_request.status_code, status.HTTP_201_CREATED)

    def test_post_validation(self):
        rating = 6
        payload = {
            'value': rating,
        }
        url = 'http://localhost:8000/character/3/rating/'
        post_request = requests.post(url=url, data=payload)
        self.assertEqual(post_request.status_code, status.HTTP_400_BAD_REQUEST)

    def test_avg_rating(self):
        """Testing if our avg function works"""
        ratings = [2, 5, 4, 4, 5]
        for rating in ratings:
            Movie.objects.create(
                character_id=3,
                value=rating
            )
        movie = Movie.objects.filter(character_id=3).latest('id')
        self.assertEqual(calculate_average(movie), 4)

    def test_max_rating(self):
        """Testing max_rate in out database"""
        ratings = [2, 5, 4, 4, 5]
        for rating in ratings:
            Movie.objects.create(
                character_id=3,
                value=rating
            )
        movie = Movie.objects.filter(character_id=3).latest('id')
        self.assertEqual(calculate_max_value(movie), 5)
