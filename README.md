## ¿Como ejecutarlo?
Instarlar docker y docker-compose, descargar djmovies e ingresar al directorio.
Ejecutar 
```bash
docker-compose up -d --build
```
API disponibles:
> GET http://localhost/character/[id]/ <br>
> POST http://localhost/character/[id]/rating/ <br>

*Adicionalmente hay que enviar la variable "value" con el valor que se quiera guardar.*
